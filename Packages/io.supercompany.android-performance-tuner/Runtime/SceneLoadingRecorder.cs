#if UNITY_2020_2_OR_NEWER
using Google.Android.PerformanceTuner;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Supercompany.AndroidPerformanceTuner
{
    public class SceneLoadingRecorder : SceneManagerAPI
    {
        protected override AsyncOperation LoadSceneAsyncByNameOrIndex(string sceneName, int sceneBuildIndex, LoadSceneParameters parameters, bool mustCompleteNextFrame)
        {
            Result<ulong> recordingResult = AndroidPerformanceTunerInitializer.PerformanceTuner.StartRecordingLoadingTime(
                new LoadingTimeMetadata
                {
                    state = LoadingTimeMetadata.LoadingState.InterLevel,
                },
                new Annotation
                {
                    Scene = (Google.Android.PerformanceTuner.Scene) sceneBuildIndex + 1,
                }
            );
            AsyncOperation result = base.LoadSceneAsyncByNameOrIndex(sceneName, sceneBuildIndex, parameters, mustCompleteNextFrame);
            if (recordingResult.errorCode == ErrorCode.Ok)
            {
                ulong handle = recordingResult.value;
                result.completed += _ =>
                {
                    AndroidPerformanceTunerInitializer.PerformanceTuner.StopRecordingLoadingTime(handle);
                };
            }
            return result;
        }
    }
}
#endif