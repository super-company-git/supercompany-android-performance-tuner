using System.Threading.Tasks;
using Google.Android.PerformanceTuner;
using UnityEngine;
using UnityEngine.SceneManagement;
using PerformanceTuner = Google.Android.PerformanceTuner.AndroidPerformanceTuner<
    Google.Android.PerformanceTuner.FidelityParams,
    Google.Android.PerformanceTuner.Annotation
>;

namespace Supercompany.AndroidPerformanceTuner
{
    public static class AndroidPerformanceTunerInitializer
    {
        public static readonly PerformanceTuner PerformanceTuner = new PerformanceTuner();

        [RuntimeInitializeOnLoadMethod]
        public static async void InitializeOnLoad()
        {
            // Games that target Vulkan should initialize late
            // Reference: https://developer.android.com/games/sdk/performance-tuner/unity/initialize-library#vulkan
            await Task.Yield();

            ErrorCode code = PerformanceTuner.Start();
            if (code != ErrorCode.Ok)
            {
                Debug.LogWarningFormat("[AndroidPerformanceTunerInitializer] Performance Tuner initialization error: {0}", code);
                return;
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            else
            {
                Debug.Log("[AndroidPerformanceTunerInitializer] Performance Tuner initialized");
            }
#endif

#if UNITY_2020_2_OR_NEWER
            SceneManagerAPI.overrideAPI = new SceneLoadingRecorder();
#endif
        }
    }
}
