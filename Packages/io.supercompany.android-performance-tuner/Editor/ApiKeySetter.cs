using System.Text.RegularExpressions;
using Google.Android.PerformanceTuner.Editor;
using UnityEditor;
using UnityEngine;

namespace Supercompany.AndroidPerformanceTuner.Editor
{
    public static class ApiKeySetter
    {
        [InitializeOnLoadMethod]
        private static void InitializeOnLoad()
        {
            if (string.IsNullOrWhiteSpace(Initializer.ProjectData.apiKey))
            {
                SetupKeyFromFirebaseConfig();
            }
        }

        [MenuItem("Tools/Supercompany Android Performance Tuner/Autofill API Key")]
        public static void SetupKeyFromFirebaseConfig()
        {
            if (GetApiKeyFromFirebaseConfig() is string apiKey)
            {
                Initializer.ProjectData.apiKey = apiKey;
            }
        }

        public static string GetApiKeyFromFirebaseConfig()
        {
            string[] guids = AssetDatabase.FindAssets("t:TextAsset google-services");
            if (guids == null || guids.Length <= 0)
            {
                return null;
            }

            foreach (string guid in guids)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);

                string firebaseConfig = AssetDatabase.LoadAssetAtPath<TextAsset>(assetPath).text;

                int packageNameIndex = firebaseConfig.IndexOf("\"package_name\": \"" + Application.identifier);
                if (packageNameIndex < 0)
                {
                    continue;
                }

                Match match = new Regex("\"current_key\": \"([^\"]*)\"").Match(firebaseConfig, packageNameIndex);
                if (match.Success)
                {
                    return match.Groups[1].Value;
                }
            }

            return null;
        }
    }
}
