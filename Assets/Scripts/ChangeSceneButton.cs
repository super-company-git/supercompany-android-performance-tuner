using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneButton : MonoBehaviour
{
    public int SceneIndex;

    public void GoToScene()
    {
        SceneManager.LoadScene(SceneIndex);
    }
}
