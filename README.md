# Supercompany Android Performance Tuner
Automatic initialization and scene load time recording using [Android Performance Tuner](https://developer.android.com/games/sdk/performance-tuner).


## Features
- No code required: just install the package and you're good to go!
- Automatic setup of API key if project uses Firebase (if `Assets/google-services.json` file exists)
- Automatic initialization of [Android Performance Tuner](https://developer.android.com/games/sdk/performance-tuner/unity/initialize-library)
- Automatically hooks a [SceneManagerAPI](https://docs.unity3d.com/ScriptReference/SceneManagement.SceneManagerAPI.html) for recording scene loading times


## How to install
1. Install [Android Performance Tuner Unity plugin](https://github.com/android/tuningfork), possibly via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) with the following URL:
   ```
   https://github.com/android/tuningfork.git#v1.5.2
   ```
2. Install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html) with the following URL:
   ```
   https://gitlab.com/super-company-git/supercompany-android-performance-tuner.git?path=Packages/io.supercompany.android-performance-tuner#1.0.0
   ```


## How to use
1. Enable the Android Performance Parameters API in Google Cloud Console, as described in the documentation: https://developer.android.com/games/sdk/performance-tuner/unity/enable-api
2. (optional) Use the global [AndroidPerformanceTunerInitializer.PerformanceTuner](Packages/io.supercompany.android-performance-tuner/Runtime/AndroidPerformanceTunerInitializer.cs) to record custom loading times or setting custom annotations and stuff
3. Enjoy 🍾